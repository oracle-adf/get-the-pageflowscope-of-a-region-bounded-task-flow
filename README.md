# Get-the-PageFlowScope-of-a-Region-Bounded-Task-Flow

### [Original code deprecated and not working with jdeveloper 12] 
You can try my codes what works
https://github.com/oracle-adf/Write-and-read-values-from-PageFlowScope-of-a-Region-Bounded-Task-Flow

<br/><br/>


Get the PageFlowScope of a Region Bounded Task Flow  
http://biemond.blogspot.ru/2011/01/get-pageflowscope-of-region-bounded.html



### Get the PageFlowScope of a Region Bounded Task Flow

    Sometimes you need to access the PageFlowScope of a Task Flow Region( child Bounded Task Flow ) and get a handle to a pageFlowScope managed Bean. Normally you don't need to do this and Oracle don't want you, to do this. To make this work you need three internal classes so there is no guarantee that it works in 11g R1 PS4 or higher, but it work in PS2 & PS3. 
    
    Basically this is what you need to do.

    Get the Task Flow binding of the region in the page definition, you need to have the full name
    Get the RootViewPortContext
    Find the ChildViewPortContext , use the TaskFlow full name
    Get the pageFlowScope Map of the ChildViewPortContext 




ADDITIONAL:

http://shidharth.blogspot.ru/2010/08/get-pageflowscope-of-any-taskflow-from.html
