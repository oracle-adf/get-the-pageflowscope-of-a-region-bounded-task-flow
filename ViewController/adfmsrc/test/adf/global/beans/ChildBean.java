package test.adf.global.beans;

import test.adf.global.interfaces.BeanInt;

public class ChildBean implements BeanInt {
    public ChildBean() {
    }

    private String name = "child";

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
