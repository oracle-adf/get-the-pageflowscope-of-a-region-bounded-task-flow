package test.adf.global.beans;

import javax.faces.event.ActionEvent;
import java.util.Map;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;

import oracle.adf.controller.internal.binding.DCTaskFlowBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adfinternal.controller.state.AdfcContext;
import oracle.adfinternal.controller.state.ChildViewPortContextImpl;
import oracle.adfinternal.controller.state.RootViewPortContextImpl;

import test.adf.global.interfaces.BeanInt;

public class MainBean {
    public MainBean() {
    }

    private String name = "main";

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void dumpPageFlowScope(ActionEvent actionEvent) {
       
      System.out.println("________________"); 
      System.out.println("dumpPageFlowScope");
        
      AdfFacesContext facesCtx= null;
      facesCtx= AdfFacesContext.getCurrentInstance();
      Map<String, Object> scopeVar= facesCtx.getPageFlowScope();
      for ( String key  : scopeVar.keySet() ) {
        System.out.println("[key]: "+key);
        System.out.println("[value]: "+scopeVar.get(key));
      }
    }

  public void dumpChildPageFlowScope(ActionEvent actionEvent) {
      
      System.out.println("________________"); 
      System.out.println("dumpChildPageFlowScope");

    // get the current BindingContainer
    BindingContext bctx = BindingContext.getCurrent();
    DCBindingContainer mainViewPageBinding = (DCBindingContainer)bctx.getCurrentBindingsEntry();

    // find the task flow pagedef binding, see the pageDef
    DCTaskFlowBinding tf = (DCTaskFlowBinding) mainViewPageBinding.findExecutableBinding("child1");
    System.out.println("[TaskFlow Full Name]: " + tf.getFullName());

    Object o1 = resolveExpression("#{controllerContext.currentViewPort}");
    RootViewPortContextImpl rootViewPort = (RootViewPortContextImpl)o1;
    ChildViewPortContextImpl childView = (ChildViewPortContextImpl) rootViewPort.getChildViewPortByClientId(tf.getFullName());

    AdfcContext afdcContext = AdfcContext.getCurrentInstance();
    
    // get pageFlowScope
    Map<String, Object> scopeVar= childView.getPageFlowScopeMap(afdcContext);
    for ( String key  : scopeVar.keySet() ) {
      System.out.println("[key]: "+key);
      System.out.println("[value]: "+scopeVar.get(key));
    }
    
    BeanInt bean = (BeanInt)scopeVar.get("childBean");
    System.out.println(bean.getName());
    
  }

  public static Object resolveExpression(String expression) {
      FacesContext facesContext = FacesContext.getCurrentInstance();
      Application app = facesContext.getApplication();
      ExpressionFactory elFactory = app.getExpressionFactory();
      ELContext elContext = facesContext.getELContext();
      ValueExpression valueExp =
          elFactory.createValueExpression(elContext, expression,
                                          Object.class);
      return valueExp.getValue(elContext);
  }

}
